package fr.afpa;

import fr.afpa.controllers.AppController;
import fr.afpa.controllers.AuthenticationController;
import fr.afpa.controllers.QuizzController;
import fr.afpa.entities.*;
import fr.afpa.utilitaries.ConstantsUtilitaries;
import fr.afpa.utilitaries.FileUtilitaries;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static final Scanner sc = new Scanner(System.in);
    private static final FileUtilitaries fileUtilitaries = new FileUtilitaries();
    private static final ConstantsUtilitaries constants = new ConstantsUtilitaries();

    private static Student student = new Student();
    private static Teacher teacher = new Teacher();

    private static ArrayList<User> teachers = new ArrayList<>();
    private static ArrayList<User> students = new ArrayList<>();
    private static ArrayList<Question> questionsQuizz = new ArrayList<>();
    private static ArrayList<Question> quizzList;

    private static AppController appController = new AppController();
    private static AuthenticationController authenticationController = new AuthenticationController();
    private static QuizzController quizzController = new QuizzController();

    private static String choice;
    private static String choiceCreateOption;
    private static String choiceCreateUser;
    private static String choiceLogIn;

    public void launcher() throws Exception {

        teachers = (ArrayList<User>) fileUtilitaries.deserialize(teachers, constants.FILE_TEACHERS);
        students = (ArrayList<User>) fileUtilitaries.deserialize(students, constants.FILE_STUDENTS);
        questionsQuizz = (ArrayList<Question>) fileUtilitaries.deserialize(questionsQuizz, constants.FILE_QUESTIONS);
        quizzList = (ArrayList<Question>) fileUtilitaries.deserialize(quizzList, constants.FILE_QUIZZ);


        appController.getMessage("Créer un compte ? [O]ui / entrée Log in");
        choiceCreateOption = sc.nextLine();

        if(choiceCreateOption.equalsIgnoreCase("O")) {
            appController.getMessage("Quel compte voulez vous créer ? [P]rofesseur / [E]tudiant");
            choiceCreateUser = sc.nextLine();

            if (choiceCreateUser.equalsIgnoreCase("P")){
                teachers.add(authenticationController.createOne(teacher));
            }

            if (choiceCreateUser.equalsIgnoreCase("E")){
                students.add(authenticationController.createOne(student));
            }

        }

        appController.getMessage("Êtes vous professeur ou étudiant ? [P]rofesseur / [E]tudiant");
        choiceLogIn = sc.nextLine();

        if (choiceLogIn.equalsIgnoreCase("P")){
           teacher = (Teacher) authenticationController.authenticate(teachers);

           do {
               appController.getMessage("<<< Bonjour professeur " + teacher.getName() + " ! >>>");
               appController.getTeacherMenu();
               choice = sc.nextLine();

               switch (choice){
                   case "1":
                       questionsQuizz.add(quizzController.newOne());
                       break;
                   case "2":
                       quizzController.displayQuestions(questionsQuizz);
                       sc.nextLine();
                       break;
                       // dev une option qui recup les resultats de l'élève
                   case "3":
                       quizzController.displayStudentQuizz(students, student);
                       sc.nextLine();
                       break;
                   case "4":
                       quizzController.displayStudents(students);
                       sc.nextLine();
                   default:
                       break;
               }

           } while(!choice.equals("0"));

        }

        if (choiceLogIn.equalsIgnoreCase("E")){
            student = (Student) authenticationController.authenticate(students);

            do {
                appController.getMessage("<<< Bonjour étudiant " + student.getName() + " ! >>>");
                appController.getStudentMenu();
                choice = sc.nextLine();

                switch (choice){
                    case "1":
                        student.getQuizz().clear();
                        quizzList = student.getQuizz();
                        quizzController.getQuestions(questionsQuizz, quizzList);
                        break;
                    case "2":
                        quizzController.displayQuizz(quizzList, student);
                        break;
                        //dev une option permettant à l'etudiant de voir ses quizz par theme
                    default:
                        break;
                }

            } while (!choice.equals("0"));
        }

        fileUtilitaries.serialize(teachers, constants.FILE_TEACHERS);
        fileUtilitaries.serialize(students, constants.FILE_STUDENTS);
        fileUtilitaries.serialize(questionsQuizz, constants.FILE_QUESTIONS);
        fileUtilitaries.serialize(quizzList, constants.FILE_QUIZZ);
    }


    public static void main(String[] args) throws Exception {
        Main mainApp = new Main();
        mainApp.launcher();
    }
}
