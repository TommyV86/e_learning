package fr.afpa.entities;

import java.io.Serializable;
import java.util.ArrayList;

public class Student extends User implements Serializable {
    private static Integer plus = 0;
    private String response;
    private Integer points;
    private ArrayList<Question> quizz = new ArrayList<>();

    public Student(){}

    public Student(String id, String name, String password){
        super(id, name, password);
    }

    public String getResponse() {
        return response;
    }
    public void setResponse(String response) {
        this.response = response;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints() {
        this.points = ++this.plus;
    }

    public ArrayList<Question> getQuizz() {
        return quizz;
    }

    public void setQuizz(ArrayList<Question> quizz) {
        this.quizz = quizz;
    }

    @Override
    public String toString() {
        return "\nStudent" +
                super.toString() +
                "\npoints = " + points +
                "\n";
    }
}
