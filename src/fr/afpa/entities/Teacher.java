package fr.afpa.entities;

import java.io.Serializable;

public class Teacher extends User implements Serializable {
    private String question;

    public Teacher(){}

    public Teacher(String id, String name, String password){
        super(id, name, password);
    }

    public String getQuestion() {
        return question;
    }
    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "Teacher :" +
                super.toString() +
                "\nquestions = " + question;
    }
}
