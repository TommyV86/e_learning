package fr.afpa.entities;

import java.io.Serializable;

public class Question implements Serializable {
    private static Integer nextId = 0;
    private Integer id;
    private String theme;
    private String title;
    private String questLine;
    private String response;


    public Question(){}
    public Question(String title, String questLine, String response, String theme) {
        this.id = ++nextId;
        this.title = title;
        this.questLine = questLine;
        this.response = response;
        this.theme = theme;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestLine() {
        return questLine;
    }

    public void setQuestLine(String questLine) {
        this.questLine = questLine;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    @Override
    public String toString() {
        return  "\n" +
                "\nQuestion :" +
                "\nid = " + id +
                "\ntheme = " + theme +
                "\ntitle = " + title +
                "\nquestLine = " + questLine +
                "\n";
    }
}
