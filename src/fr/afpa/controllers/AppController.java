package fr.afpa.controllers;

import fr.afpa.services.IAppService;
import fr.afpa.services.implementations.AppServiceIMP;

public class AppController {
    private final IAppService appService = new AppServiceIMP();

    public void getTeacherMenu(){
        appService.getTeacherMenu();
    }

    public void getStudentMenu(){
        appService.getStudentMenu();
    }

    public void getMessage(String message){
        appService.getMessage(message);
    }
}
