package fr.afpa.controllers;

import fr.afpa.entities.Question;
import fr.afpa.entities.Student;
import fr.afpa.entities.User;
import fr.afpa.services.IQuizzService;
import fr.afpa.services.implementations.QuizzServiceIMP;

import java.util.ArrayList;
import java.util.Scanner;

public class QuizzController {
    private final Scanner sc = new Scanner(System.in);
    private final IQuizzService quizzServiceIMP = new QuizzServiceIMP();

    public Question newOne(){
        System.out.println("=== Création question ===");

        System.out.println("Saisir le thème : ");
        String theme = sc.nextLine();

        System.out.println("saisir titre : ");
        String title = sc.nextLine();

        System.out.println("saisir la question : ");
        String questLine = sc.nextLine();

        System.out.println("saisir la reponse : ");
        String response = sc.nextLine();

        Question question = new Question(title, questLine, response, theme);
        return quizzServiceIMP.newOne(question);

    }

    public void displayQuestions(ArrayList<Question> questions){
        quizzServiceIMP.getAllQuestions(questions);
    }

    public void getQuestions(ArrayList<Question> questions, ArrayList<Question> quizzList){
        System.out.println("=== Chercher un quizz par theme ===");
        System.out.println("Entrer un theme : alimentation - manga - temps");
        String theme = (sc.nextLine());
        quizzServiceIMP.getQuestions(questions, quizzList, theme);
    }

    public void displayQuizz(ArrayList<Question> questionsQuizz, Student student) {
        quizzServiceIMP.getQuizz(questionsQuizz, student);
    }

    public void displayStudentQuizz(ArrayList<User> students, Student student){
        System.out.println("entrer un id : ");
        String id = sc.nextLine();
        quizzServiceIMP.getStudentQuizz(students, student, id);
    }

    public void displayStudents(ArrayList<User> students) {
        quizzServiceIMP.getStudents(students);
    }
}
