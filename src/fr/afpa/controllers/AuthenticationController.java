package fr.afpa.controllers;

import fr.afpa.entities.Student;
import fr.afpa.entities.Teacher;
import fr.afpa.entities.User;
import fr.afpa.services.IAuthenticationService;
import fr.afpa.services.implementations.AuthenticationServiceIMP;
import fr.afpa.utilitaries.ClassUtilitaries;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class AuthenticationController {

    private final Scanner sc = new Scanner(System.in);
    private final Random ran = new Random();
    private final ClassUtilitaries classUtilitaries = new ClassUtilitaries();
    private final IAuthenticationService authenticationService = new AuthenticationServiceIMP();
    private User newUser;

    public User createOne(User userPrompt) throws Exception {

        System.out.println("=== Création compte ===");

        System.out.println("saisir nom");
        String userName = sc.nextLine();

        System.out.println("saisir mot de passe : ");
        String password = sc.nextLine();

        if (userPrompt instanceof Student) {
            String id = classUtilitaries.generateId(ran, 3, "ST", true);
            newUser = new Student(id, userName, password);
        }

        if (userPrompt instanceof Teacher) {
            String id = classUtilitaries.generateId(ran, 3, "TE", true);
            newUser = new Teacher(id, userName, password);
        }

        return authenticationService.newAccount(newUser);
    }

    public User authenticate(ArrayList<User> users) throws Exception {
        System.out.println("=== Connexion ===");

        System.out.println("saisir le nom : ");
        String userName = sc.nextLine();

        System.out.println("saisir le mot de passe : ");
        String password = sc.nextLine();

        return authenticationService.authenticate(users, userName, password);
    }
}
