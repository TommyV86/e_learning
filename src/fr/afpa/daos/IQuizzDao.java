package fr.afpa.daos;

import fr.afpa.entities.Question;
import fr.afpa.entities.Student;
import fr.afpa.entities.User;

import java.util.ArrayList;

public interface IQuizzDao {
    Question newOne(Question question);
    void getAllQuestions(ArrayList<Question> questions);
    void getQuestions(ArrayList<Question> questions, ArrayList<Question> quizzList, String theme);
    void getQuizz(ArrayList<Question> questions, Student student);
    void getStudentQuizz(ArrayList<User> students,Student student, String id);
    void getStudents(ArrayList<User> students);
}
