package fr.afpa.daos.implementations;

import fr.afpa.daos.IAuthenticationDao;
import fr.afpa.entities.User;

import java.util.ArrayList;

public class AuthenticationDaoIMP implements IAuthenticationDao {

    @Override
    public User newAccount(User user) {
        return user;
    }
    @Override
    public User authenticate(ArrayList<User> users, String userName, String password) {

        for (User currentUser: users) {
            if (currentUser.getName().equals(userName) &&
                    currentUser.getPassword().equals(password)) {
                return currentUser;
            }
        }
        return null;
    }
}
