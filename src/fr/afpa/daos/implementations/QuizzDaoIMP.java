package fr.afpa.daos.implementations;

import fr.afpa.daos.IQuizzDao;
import fr.afpa.entities.Question;
import fr.afpa.entities.Student;
import fr.afpa.entities.User;
import fr.afpa.utilitaries.ClassUtilitaries;

import java.util.ArrayList;

public class QuizzDaoIMP implements IQuizzDao {
    private static ClassUtilitaries classUtilitaries = new ClassUtilitaries();

    @Override
    public Question newOne(Question question) {
        return question;
    }

    @Override
    public void getAllQuestions(ArrayList<Question> questions) {
        for (Question currentQuestion : questions) {
            classUtilitaries.getMessage(currentQuestion.toString());
        }
    }

    @Override
    public void getQuestions(ArrayList<Question> questions, ArrayList<Question> quizzList, String theme) {

        for (Question currentQuestion : questions) {
            if(currentQuestion.getTheme().equals(theme)){
                quizzList.add(currentQuestion);
            }
        }
    }

    @Override
    public void getQuizz(ArrayList<Question> questions, Student student) {
        for (Question currentQuestion : questions) {
            System.out.println(currentQuestion.toString());
            student.setResponse(classUtilitaries.response());
            if (student.getResponse().equalsIgnoreCase(currentQuestion.getResponse())){
                student.setPoints();
            }
        }
        classUtilitaries.getMessage("\n*** score : " + student.getPoints() + " ***\n");
    }

    @Override
    public void getStudentQuizz(ArrayList<User> students, Student student, String id) {
        for (User currentUser : students) {
            if (currentUser.getId().equalsIgnoreCase(id)){
                student = (Student) currentUser;
                classUtilitaries.getMessage(student.toString());
                classUtilitaries.getMessage(student.getQuizz().toString());
            }
        }
    }

    @Override
    public void getStudents(ArrayList<User> students) {
        for (User currentStudent : students) {
            classUtilitaries.getMessage(currentStudent.toString());
        }
    }
}
