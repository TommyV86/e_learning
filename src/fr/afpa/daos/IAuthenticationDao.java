package fr.afpa.daos;

import fr.afpa.entities.User;

import java.util.ArrayList;

public interface IAuthenticationDao {
    User newAccount(User user) throws Exception;
    User authenticate(ArrayList<User> users, String userName, String password) throws Exception;

}
