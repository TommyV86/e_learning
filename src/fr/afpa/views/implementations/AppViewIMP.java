package fr.afpa.views.implementations;

import fr.afpa.views.IAppView;

public class AppViewIMP implements IAppView {
    @Override
    public void getTeacherMenu() {
        System.out.println("****** e_learning *******");
        System.out.println("- interface professeur -");
        System.out.println();
        System.out.println("\t [1] - créer un quizz ");
        System.out.println("\t [2] - afficher les questions ");
        System.out.println("\t [3] - afficher un étudiant ");
        System.out.println("\t [4] - afficher les étudiants ");
        System.out.println("\t [0] - fermer l'application ");
        System.out.println();
        System.out.println("**************************************");
    }

    public void getStudentMenu(){
        System.out.println("****** e_learning *******");
        System.out.println("- interface étudiant -");
        System.out.println();
        System.out.println("\t [1] - choisir un quizz par thème");
        System.out.println("\t [2] - lancer un quizz");
        System.out.println("\t [0] - fermer l'application ");
        System.out.println();
        System.out.println("**************************************");
    }

    public void getMessage(String message) {
        System.out.println(message);
    }
}
