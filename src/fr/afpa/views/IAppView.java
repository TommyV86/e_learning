package fr.afpa.views;

public interface IAppView {
    void getTeacherMenu();
    void getStudentMenu();
    void getMessage(String message);
}
