package fr.afpa.services;

public interface IAppService {
    void getTeacherMenu();
    void getStudentMenu();
    void getMessage(String message);
}
