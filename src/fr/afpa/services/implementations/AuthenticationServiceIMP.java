package fr.afpa.services.implementations;

import fr.afpa.daos.IAuthenticationDao;
import fr.afpa.daos.implementations.AuthenticationDaoIMP;
import fr.afpa.entities.User;
import fr.afpa.services.IAuthenticationService;

import java.util.ArrayList;

public class AuthenticationServiceIMP implements IAuthenticationService {

    private IAuthenticationDao authenticationDao = new AuthenticationDaoIMP();

    @Override
    public User authenticate(ArrayList<User> users, String userName, String password) throws Exception {

        return authenticationDao.authenticate(users, userName, password);

    }

    @Override
    public User newAccount(User userPrompt) throws Exception {
        return authenticationDao.newAccount(userPrompt);
    }
}
