package fr.afpa.services.implementations;

import fr.afpa.services.IAppService;
import fr.afpa.views.IAppView;
import fr.afpa.views.implementations.AppViewIMP;

public class AppServiceIMP implements IAppService {
    private final IAppView appView = new AppViewIMP();
    @Override
    public void getTeacherMenu() {
        appView.getTeacherMenu();
    }
    public void getStudentMenu(){
        appView.getStudentMenu();
    }
    public void getMessage(String message){
        appView.getMessage(message);
    }
}
