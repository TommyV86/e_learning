package fr.afpa.services.implementations;

import fr.afpa.daos.IQuizzDao;
import fr.afpa.daos.implementations.QuizzDaoIMP;
import fr.afpa.entities.Question;
import fr.afpa.entities.Student;
import fr.afpa.entities.User;
import fr.afpa.services.IQuizzService;

import java.util.ArrayList;

public class QuizzServiceIMP implements IQuizzService {
    IQuizzDao quizzDaoIMP = new QuizzDaoIMP();
    @Override
    public Question newOne(Question question) {
        return quizzDaoIMP.newOne(question);
    }

    @Override
    public void getAllQuestions(ArrayList<Question> questions) {
         quizzDaoIMP.getAllQuestions(questions);
    }

    @Override
    public void getQuestions(ArrayList<Question> questions, ArrayList<Question> quizzList, String theme) {
        quizzDaoIMP.getQuestions(questions, quizzList, theme);
    }

    @Override
    public void getQuizz(ArrayList<Question> quizz, Student student) {
        quizzDaoIMP.getQuizz(quizz, student);
    }

    @Override
    public void getStudentQuizz(ArrayList<User> students,Student student, String id) {
        quizzDaoIMP.getStudentQuizz(students, student, id);
    }

    @Override
    public void getStudents(ArrayList<User> students) {
        quizzDaoIMP.getStudents(students);
    }
}
