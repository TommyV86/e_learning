package fr.afpa.services;


import fr.afpa.entities.User;

import java.util.ArrayList;

public interface IAuthenticationService {
    User authenticate(ArrayList<User> users, String userName, String password) throws Exception;
    User newAccount(User userPrompt) throws Exception;
}
