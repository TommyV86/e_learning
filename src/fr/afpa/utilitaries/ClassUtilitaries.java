package fr.afpa.utilitaries;

import java.util.Random;
import java.util.Scanner;

public class ClassUtilitaries {
    public String generateId(Random ran, int nbrElements, String alpha, Boolean isAlphanumeric) {
        String ALPHA = alpha;

        String code = "";
        String randomNumbers = "";

        for(int i = 1; i <= nbrElements; i++) {
            randomNumbers += String.valueOf(ran.nextInt(9));
        }

        if(isAlphanumeric) {
            code += ALPHA + randomNumbers;
            return code;
        }

        code += randomNumbers;
        return code;
    }

    public String response(){
        Scanner sc = new Scanner(System.in);
        String res = sc.nextLine();
        return res;
    }

    public void getMessage(String message) {
        System.out.println(message);
    }
}
