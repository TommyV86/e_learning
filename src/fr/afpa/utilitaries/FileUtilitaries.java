package fr.afpa.utilitaries;

import fr.afpa.entities.Student;
import fr.afpa.entities.Teacher;

import java.io.*;
import java.util.ArrayList;

public class FileUtilitaries {

    private static final ConstantsUtilitaries constants = new ConstantsUtilitaries();

    public void serialize(ArrayList<?> users, String fileName){

        try {
            String filename = constants.FILE_PATH + fileName + constants.SER_EXT;
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);

            out.writeObject(users);

            out.close();
            file.close();

            System.out.println("données sérialisées");

        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public ArrayList<?> deserialize(ArrayList<?> users , String fileName){
        try {
            String filename = constants.FILE_PATH + fileName + constants.SER_EXT;
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);

            users = (ArrayList<?>) in.readObject();

            in.close();
            file.close();

            System.out.println("données déserialisés ");


        } catch(IOException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return users;
    }
}
