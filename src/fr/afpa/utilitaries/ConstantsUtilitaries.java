package fr.afpa.utilitaries;

public class ConstantsUtilitaries {
    public static final String FILE_PATH = "../e_learning/src/fr/afpa/datas/";
    public static final String SER_EXT = ".ser";
    public static final String FILE_QUIZZ = "quizz";
    public static final String FILE_QUESTIONS = "questions";
    public static final String FILE_TEACHERS = "teachers";
    public static final String FILE_STUDENTS = "students";


}
